import React, {useEffect, useState} from "react";
import Card from "../components/Products/Card";
import '../pages/FavouritePage.scss'



export default function FavouritePage({ initialFavourite, removeFromFavourite, addToFavourite }){
    const [favourite, setFavourite] = useState(initialFavourite ? initialFavourite : []);
    const [products, setProducts] = useState([]);


    useEffect(() => {

        fetch('/products.json')
            .then(response => response.json())
            .then(products =>{

                setProducts(favourite.map(id => products.filter(product => product.id === id)[0]));
                console.log(initialFavourite);
                console.log(favourite.map(id => products.filter(product => product.id === id)[0]))
            })
    }, [favourite])

    // useEffect(()=>{
    //
    // }, [favourite])


    const cards = products.map((product, index) => {
        const isCardInFavourite = favourite.includes(product.id)
        return <Card
            key={index}
            removeFromFavourite={(id) => {removeFromFavourite(id); setFavourite(favourite.filter(f => f!==id));}}
            favourite={initialFavourite}
            product={product}
            addToFavourite = {addToFavourite}
            isCardInFavourite = {isCardInFavourite}
            hideAddButton={true}


        />
    });

    return(
        <div className='favourite-container'>
            {products.length !== 0 &&
            <div className='favourite-title-wrapper'>
                <h2 className='favourite-title'>Обране</h2>
            </div>}

            <div className='favourite-items'>
                {products.length !==0 ?
                    cards
                    : (<>
                        <div className="empty">
                            <h3 className="empty-title">Немає обраних</h3>
                        </div>
                    </>)
                }
            </div>
        </div>

    )
}
