import React from "react";
import { Outlet } from "react-router-dom";
import Header from "../../components/Header/Header";

export default function Layout ({countBasket, countFavourite }) {
    return(
       <>
    <Header countBasket={countBasket} countFavourite={countFavourite} ></Header>
    <Outlet/>
       </>
        )
}

