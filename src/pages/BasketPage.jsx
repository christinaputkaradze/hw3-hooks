import React, {useEffect, useState} from "react";
import basket from "../components/Header/Basket";
import Card from "../components/Products/Card";
import "../pages/BasketPage.scss"

export default function BasketPage({ initialBasket, removeFromBasket }){
    const [basket, setBasket] = useState(initialBasket ? initialBasket : []);
    const [products, setProducts] = useState([]);


    useEffect(() => {

        fetch('/products.json')
            .then(response => response.json())
            .then(products =>{

                setProducts(basket.map(id => products.filter(product => product.id === id)[0]));
            });

    }, []);

    function removeFromProducts (pr, id){
        let newProductIDs = [];

        pr.forEach((item) => {
            newProductIDs.push(item.id);
        });

        newProductIDs.splice(newProductIDs.indexOf(id), 1);

        let newProducts = newProductIDs.map(id => products.filter(product => product.id === id)[0]);
        return newProducts;
    }

    const cards = products.map((product, index) => {

        let modalSettings = {header: "Видалити товар з кошика?",
            actionOnConfirm: (id) => {
                removeFromBasket(id);
                setProducts(removeFromProducts(products, id));
            }
        };

        return <Card
            key={index}
            basket={initialBasket}
            product={product}
            hideHeart={true}
            modalSettings={modalSettings}
            buttonText={'Видалити товар'}
        />
    });

    return(
        <div className='basket-container'>
            {products.length !== 0 &&
            <div className='basket-title-wrapper'>
                <h2 className='basket-title'>Ваше замовлення</h2>
            </div>
            }

            <div className='basket-items'>
                {products.length !==0 ?
                cards
            : (<>
                    <div className="empty">
                        <h3 className="epmty-title">Кошик порожній</h3>
                     </div>
            </>)
            }
            </div>
        </div>

    )
}

