import React from "react";
import Header from "../components/Header/Header";
import Products from "../components/Products/Products";

export default function MainPage({ favourite, basket, addToBasket, addToFavourite, removeFromFavourite }){
    return(
        <div className="App">
            <Products addToBasket={addToBasket}
                      basket ={basket}
                      favourite={favourite}
                      addToFavourite = {addToFavourite}
                      removeFromFavourite = {removeFromFavourite}
            />
        </div>
    )
}
