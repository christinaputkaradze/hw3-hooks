import React, {useState} from 'react';
import Header from './components/Header/Header';
import Products from './components/Products/Products';
import {BrowserRouter, Route, Routes} from "react-router-dom";
import MainPage from "./pages/MainPage";
import FavouritePage from "./pages/FavouritePage";
import BasketPage from "./pages/BasketPage";
import Layout from "./pages/Layout/Layout";


export default function App () {
    // const [modal, setModal] = useState(false);
    const [basket, setBasket] = useState(localStorage.getItem('basket')  ? JSON.parse(localStorage.getItem('basket')) : []);
    const [favourite, setFavourite] = useState(localStorage.getItem('favourite') ? JSON.parse(localStorage.getItem('favourite')) : []);

    const addToBasket=(id)=>{
        let newBasket = [...basket, id];

        setBasket(newBasket);
        localStorage.setItem('basket', JSON.stringify(newBasket));
    }

    const addToFavourite = (id) => {
        let newFavourite = [...favourite, id];

        setFavourite(newFavourite);
        localStorage.setItem('favourite', JSON.stringify(newFavourite));
    }

    const removeFromBasket = (id) => {
        let newBasket = [];

        basket.forEach((item) => {
            newBasket.push(item)
        });

        newBasket.splice(newBasket.indexOf(id), 1);

        setBasket(newBasket);
        localStorage.setItem('basket', JSON.stringify(newBasket));
    }

    const removeFromFavourite = (id) => {
        let newFavourite = favourite.filter((favourite) => favourite !== id);

        setFavourite(newFavourite);
        localStorage.setItem('favourite', JSON.stringify(newFavourite));
    }


    return(
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<Layout countFavourite={favourite.length} countBasket={basket.length}/>}>
                    <Route index element={<MainPage favourite={favourite} addToFavourite={addToFavourite} addToBasket={addToBasket} basket={basket} removeFromFavourite={removeFromFavourite}/>}/>
                    <Route path="/favourite" element={<FavouritePage initialFavourite={favourite} removeFromFavourite={removeFromFavourite}/>} />
                     <Route path="/basket" element={<BasketPage initialBasket={basket} removeFromBasket={removeFromBasket}/>} />
                </Route>
            </Routes>

        </BrowserRouter>
    )
}

