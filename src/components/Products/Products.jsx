import React, {Component, useEffect, useState} from "react";
import '../Products/products.scss'

import Card from "./Card";

export default function Products (props){
    const [products, setProducts] = useState([]);

    useEffect(() => {

        fetch('/products.json')
            .then(response => response.json())
            .then(products =>{

                setProducts(products);

            })
    }, [])


    const cards = products.map(product =>{
        const isCardInBasket = props.basket.includes(product.id);
        const isCardInFavourite = props.favourite.includes(product.id);
        let modalSettings = {header: "Додати товар до кошика?", actionOnConfirm: props.addToBasket};

        return <Card
            key={product.id.toString()}
            addToBasket={props.addToBasket}
            removeFromBasket = {props.removeFromBasket}
            basket={props.basket}
            product ={product}
            isCardInBasket = {isCardInBasket}
            favourite= {props.favourite}
            addToFavourite = {props.addToFavourite}
            removeFromFavourite = {props.removeFromFavourite}
            isCardInFavourite = {isCardInFavourite}
            modalSettings = {modalSettings}
            buttonText={"Додати в кошик"}
        />
    })

    return(
        <div className="products">
            <div className="products-container">
                <div className="products">
                    {cards}
                </div>

            </div>


        </div>
    )
}




