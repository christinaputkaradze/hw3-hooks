import React, {useState} from "react";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import '../Products/card.scss'

export default function Card (props){

    let hideAddButton = props.hideAddButton ? true : false;
    let hideHeart = props.hideHeart ? true : false;
    let modalSettings = props.modalSettings;


    //todo: const [favourite, setFavourite] = useState(false);
    const [modal, setModal] = useState(false);

    const showModal = () => {
        setModal(true);
    };

    const closeModal = () => {
        setModal(false);
    };

    const addToFavourite = () => {
        if (props.isCardInFavourite) {
            props.removeFromFavourite(props.product.id);
        } else {
            props.addToFavourite(props.product.id);
        }
    };


    const { art, url, name, color, price } = props.product;

    const actionsModal = (
        <div className="modal-footer">
            <Button
                text="Так"
                onClick={() => {
                    modalSettings.actionOnConfirm(props.product.id);
                    closeModal();}
            }
                className="modal-button"
            />
            <Button
                text="Ні"
                onClick={closeModal}
                className="modal-button"
            />
        </div>
    );

    return (
        <div className="card">
            <div className="card-img-wrapper">
                <img className="card-img" src={url} alt="img" />
            </div>
            <div className="card-content">
                <p className="card-title">{name}</p>
                <p className="card-articul">{art}</p>
                <p className="card-color">{color}</p>
                <div className="price-wrapper">
                    <p className="card-price">{price}</p>
                    {hideHeart !== true &&(
                    <div
                        className="favourite-icon-wrapper"
                        onClick={addToFavourite}
                    >
                        <img
                            className="favourite-icon"
                            src={
                                props.isCardInFavourite
                                    ? "/icons/heart-329.svg"
                                    : "/icons/heart-492.svg"
                            }
                            alt="heart"
                        />
                    </div>
                    ) }
                </div>
            </div>

            <div className="card-footer">
                {hideAddButton !== true &&(
                <Button
                    className="card-button"
                    onClick={(e) => showModal(e)}
                    text={props.buttonText}
                />)}
                {modal && (
                    <Modal
                        closeButton={true}
                        header={modalSettings.header}
                        actions={actionsModal}
                        onClick={(e) => closeModal(e)}
                    />
                )}
            </div>
        </div>
    );
}



