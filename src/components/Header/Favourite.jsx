import React from "react";


const Favourite = (props) =>{
        const {countFavourite} = props;
        return (
            <div className='favourite-wrapper'>
                <img
                    className="favourite-img"
                    src='/icons/heart-329.svg'
                />
                <p className='favourite-count'>{countFavourite}</p>
            </div>
        );
    }

export default Favourite;