import React from "react";
import { NavLink } from "react-router-dom";
import Favourite from "./Favourite";
import Basket from "./Basket";
import '../Header/header.scss'

export default function Header ({ countBasket, countFavourite }){
    return(
        <header className='header-wrapper'>
            <div className='header'>
                <div className='header-icons-container'>
                    <NavLink to="/">
                        <img
                            className="site-logo"
                            src="https://kachorovska.com/image/logo_new.svg"
                            alt="logo"
                        />
                    </NavLink>
                    <div className='header-right'>
                        <NavLink to="/favourite">
                            <Favourite countFavourite={countFavourite}/>
                        </NavLink>
                        <NavLink to="/basket">
                            <Basket countBasket={countBasket}/>
                        </NavLink>

                </div>
                </div>
            </div>
        </header>
    )
}

