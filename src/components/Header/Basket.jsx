import React from "react";

const Basket = (props) => {
    const {countBasket} = props
    return (
        <div className='basket-wrapper'>
            <img
                className="basket-img"
                src='/icons/shopping-cart-3041.svg'
            />
            <p className='basket-count'>{countBasket}</p>
        </div>
    )
}
export default Basket;