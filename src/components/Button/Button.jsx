import React from "react";
import "./style.scss"

export default function Button({ text, onClick, className }) {
    return (
        <button
            onClick={onClick}
            className={className}>
            {text}
        </button>
    );
}

Button.defaulyProps = {
    type: 'button',
}




