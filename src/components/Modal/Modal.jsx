import "./style.scss";
import {useEffect, useRef} from "react";

export default function Modal ({ header, closeButton, actions, onClick }){
    const modalRef = useRef();

    useEffect(() => {
        function handleOutsideClick(event) {
            if (modalRef.current && !modalRef.current.contains(event.target)) {
                onClick();
            }
        }

        document.addEventListener('mousedown', handleOutsideClick);


        return () => {
            document.removeEventListener('mousedown', handleOutsideClick);
        };
    }, [onClick]);

        return (
        <>
            <div className="modal-wrapper">
                <div ref={modalRef} className="modal-container">
                    <div className="modal">
                        <div className="modal-header">
                            <h2 className="modal-title">{header}</h2>
                            {closeButton && <button
                                className={'modal-close'}
                                onClick={onClick}>
                                Х
                            </button>}
                        </div>
                        <div className="modal-content">
                            <div className="modal-btns">{actions}</div>
                        </div>
                    </div>
                </div>
            </div>
        </>

    );
}


